import React, { useState } from 'react';
import Chart from 'react-apexcharts';
import {
  XYPlot,
  XAxis,
  YAxis,
  VerticalGridLines,
  HorizontalGridLines,
  AreaSeries,
  Crosshair,
  Hint
} from 'react-vis';

export default function GraphArea(props) {
  /* const [show, setShow] = useState(false);

  const layers = [
   [
      {x: 1, y: 5}, 
      {x: 2, y: 7}, 
      {x: 3, y: 10},
      {x: 4, y: 18}
    ],
   [
      {x: 1, y: 31}, 
      {x: 2, y: 27}, 
      {x: 3, y: 30},
      {x: 4, y: 23}
    ],
   [
      {x: 1, y: 45}, 
      {x: 2, y: 47}, 
      {x: 3, y: 50},
      {x: 4, y: 53}
    ],
   [
      {x: 1, y: 61}, 
      {x: 2, y: 77}, 
      {x: 3, y: 80},
      {x: 4, y: 63}
    ],
   [
      {x: 1, y: 81}, 
      {x: 2, y: 97}, 
      {x: 3, y: 90},
      {x: 4, y: 100}
    ],
   [
      {x: 1, y: 101}, 
      {x: 2, y: 117}, 
      {x: 3, y: 110},
      {x: 4, y: 103}
    ],
   [
      {x: 1, y: 121}, 
      {x: 2, y: 137}, 
      {x: 3, y: 140},
      {x: 4, y: 133}
    ],
   [
      {x: 1, y: 151}, 
      {x: 2, y: 147}, 
      {x: 3, y: 140},
      {x: 4, y: 153}
    ],
   [
      {x: 1, y: 161}, 
      {x: 2, y: 177}, 
      {x: 3, y: 180},
      {x: 4, y: 163}
    ],
     [
      {x: 1, y: 181}, 
      {x: 2, y: 187}, 
      {x: 3, y: 200},
      {x: 4, y: 190}
    ]
  ];

  const showHint = () => {
    setShow(true);
  } */

  const apexChartState1 = {
    series: [
      {
        name: 'layer 1',
        data: [{x: 1, y: 181}, 
          {x: 2, y: 187}, 
          {x: 3, y: 200},
          {x: 4, y: 190}]
      },
      {
        name: 'layer2',
        data: [{x: 1, y: 161}, 
          {x: 2, y: 177}, 
          {x: 3, y: 180},
          {x: 4, y: 163}]
      },
      {
        name: 'layer3',
        data: [{x: 1, y: 151}, 
          {x: 2, y: 147}, 
          {x: 3, y: 140},
          {x: 4, y: 153}]
      },
      {
        name: 'layer4',
        data: [{x: 1, y: 121}, 
          {x: 2, y: 137}, 
          {x: 3, y: 140},
          {x: 4, y: 133}]
      },
      {
        name: 'layer5',
        data: [{x: 1, y: 101}, 
          {x: 2, y: 117}, 
          {x: 3, y: 110},
          {x: 4, y: 103}]
      },
      {
        name: 'layer6',
        data: [{x: 1, y: 81}, 
          {x: 2, y: 97}, 
          {x: 3, y: 90},
          {x: 4, y: 100}]
      },
      {
        name: 'layer7',
        data: [{x: 1, y: 61}, 
          {x: 2, y: 77}, 
          {x: 3, y: 80},
          {x: 4, y: 63}]
      },
      {
        name: 'layer8',
        data: [{x: 1, y: 45}, 
          {x: 2, y: 47}, 
          {x: 3, y: 50},
          {x: 4, y: 53}]
      },
      {
        name: 'layer9',
        data: [{x: 1, y: 31}, 
          {x: 2, y: 27}, 
          {x: 3, y: 30},
          {x: 4, y: 23}]
      },
      {
        name: 'layer10',
        data: [{x: 1, y: 5}, 
          {x: 2, y: 7}, 
          {x: 3, y: 10},
          {x: 4, y: 18}]
      }
    ],
    options: {
      chart: {
        type: 'area',
        height: 220,
        //stacked: true,
        /* events: {
          selection: function (chart, e) {
            console.log(new Date(e.xaxis.min))
          }
        }, */
      },
      colors: ['#008FFB', '#00E396', '#CED4DC'],
      dataLabels: {
        enabled: false,
        formatter: function (val, opts) {
          return `${val} cm`
        },
      },
      stroke: {
        curve: 'smooth'
      },
      fill: {
        type: 'gradient',
        gradient: {
          opacityFrom: 1,
          opacityTo: 1,
        }
      },
      /* legend: {
        position: 'top',
        horizontalAlign: 'left'
      }, */
      xaxis: {
        //type: 'datetime'
        labels: {
          formatter: function (val, opts) {
            return `${val} m`
          },
        },
        title: {
          text: 'profile width'
        }
      },
      yaxis: {
        labels: {
          formatter: function (val, opts) {
            return `${val} cm`
          },
        },
        title: {
          text: 'profile depth'
        }
      },
      tooltip: {
        shared: false,
        custom: function({series, seriesIndex, dataPointIndex, w}) {
          console.log(series, seriesIndex, dataPointIndex, w)
          console.log(series[seriesIndex])
          return '<div class="arrow_box">' +
            '<span>' + series[seriesIndex][dataPointIndex] + '</span>' +
            '</div>'
        }
      },
    },
  }

  const apexChartState2 = {
    series: [
      {
        name: 'layer 1',
        data: [{x: 1, y: 10}, 
          {x: 2, y: 7}, 
          {x: 3, y: 20},
          {x: 4, y: 10}]
      },
      {
        name: 'layer2',
        data: [{x: 1, y: 1}, 
          {x: 2, y: 17}, 
          {x: 3, y: 20},
          {x: 4, y: 3}]
      },
      {
        name: 'layer3',
        data: [{x: 1, y: 11}, 
          {x: 2, y: 7}, 
          {x: 3, y: 0},
          {x: 4, y: 13}]
      },
      {
        name: 'layer4',
        data: [{x: 1, y: 8}, 
          {x: 2, y: 7}, 
          {x: 3, y: 20},
          {x: 4, y: 13}]
      },
      {
        name: 'layer5',
        data: [{x: 1, y: 15}, 
          {x: 2, y: 17}, 
          {x: 3, y: 18},
          {x: 4, y: 3}]
      },
      {
        name: 'layer6',
        data: [{x: 1, y: 1}, 
          {x: 2, y: 17}, 
          {x: 3, y: 10},
          {x: 4, y: 20}]
      },
      {
        name: 'layer7',
        data: [{x: 1, y: 11}, 
          {x: 2, y: 17}, 
          {x: 3, y: 20},
          {x: 4, y: 3}]
      },
      {
        name: 'layer8',
        data: [{x: 1, y: 5}, 
          {x: 2, y: 7}, 
          {x: 3, y: 10},
          {x: 4, y: 13}]
      },
      {
        name: 'layer9',
        data: [{x: 1, y: 13}, 
          {x: 2, y: 7}, 
          {x: 3, y: 10},
          {x: 4, y: 13}]
      },
      {
        name: 'layer10',
        data: [{x: 1, y: 5}, 
          {x: 2, y: 7}, 
          {x: 3, y: 15},
          {x: 4, y: 18}]
      }
    ],
    options: {
      chart: {
        type: 'area',
        height: 220,
        stacked: true,
        /* events: {
          selection: function (chart, e) {
            console.log(new Date(e.xaxis.min))
          }
        }, */
      },
      colors: ['#008FFB', '#00E396', '#CED4DC'],
      dataLabels: {
        enabled: false
      },
      stroke: {
        curve: 'smooth'
      },
      fill: {
        type: 'gradient',
        gradient: {
          opacityFrom: 1,
          opacityTo: 1,
        }
      },
      /* legend: {
        position: 'top',
        horizontalAlign: 'left'
      }, */
      xaxis: {
        //type: 'datetime'
      },
      tooltip: {
        shared: false,
      }
    },
  }
  
  return (
    <div className="graph-area">
      {/* <XYPlot width={300} height={400}>
        <VerticalGridLines />
        <HorizontalGridLines />
        <XAxis />
        <YAxis />
        {layers.reverse().map((layer, index) => 
            <AreaSeries
            className="area-series-example"
            curve="curveNatural"
            data={layer}
            //onNearestX={showHint}
            onSeriesClick={showHint}
            onNearestX={(datapoint, event)=>{
              console.log(datapoint, event)
            }}
          />
        )}
        {show &&
          <Hint value={['niach']} />
        }
        
      </XYPlot> */}
      <Chart options={apexChartState1.options} series={apexChartState1.series.reverse()} type="area" height={440} />
      {/* <Chart options={apexChartState2.options} series={apexChartState2.series} type="area" height={440} /> */}
    </div>
  );
}