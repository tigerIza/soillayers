import React, { useState, useCallback } from 'react'
import { GoogleMap, LoadScript, Marker, Polygon } from '@react-google-maps/api';
 
const containerStyle = {
  width: '400px',
  height: '400px'
};
 
const stCenter = {
  lat: -34.8714203,
	lng: 150.3591997
};

const polygonData = [
	{lat: -34.839021, lng: 150.429753},
	{lat: -34.844939, lng: 150.428379},
	{lat: -34.844818, lng: 150.421664},
	{lat: -34.911660, lng: 150.412018},
	{lat: -34.916462, lng: 150.388074},
	{lat: -34.873229, lng: 150.363786},
	{lat: -34.827661, lng: 150.404425},
	{lat: -34.837188, lng: 150.410892},
	{lat: -34.839021, lng: 150.429753},
]
 
function MapWrapper(props) {
	const [map, setMap] = useState(null);
	const [center, setCenter] = useState(stCenter);

	const showGraph = () => {
		if (map) {
			props.areaClicked(true);
		}
	}
 
  const onLoad = useCallback(function callback(map) {
    /* const bounds = new window.google.maps.LatLngBounds(center);
    map.fitBounds(bounds); */
		setMap(map)
		console.log(map)

		const geocoder = new window.google.maps.Geocoder();
		geocoder.geocode({'address': "Buangla NSW 2540"}, function(results, status) {
			if (status === 'OK') {
				setCenter(results[0].geometry.location);
			} else {
				alert('Geocode was not successful for the following reason: ' + status);
			}
		});
  }, [])
 
  const onUnmount = useCallback(function callback(map) {
    setMap(null)
  }, [])
 
  return (
		<div className="map">
			<LoadScript
				googleMapsApiKey="AIzaSyAjjWFEYJmxdnBcZrlL94gnzVoesbdhmrA"
			>
				<GoogleMap
					mapContainerStyle={containerStyle}
					center={center}
					zoom={11}
					onLoad={onLoad}
					onUnmount={onUnmount}
				>
					<Marker></Marker>
					<Polygon paths={polygonData}
						strokeColor="#0000FF"
						strokeOpacity={0.8}
						strokeWeight={2}
						fillColor="#0000FF"
						fillOpacity={0.35}
						onClick={showGraph} />
				</GoogleMap>
			</LoadScript>
		</div>
  )
}
 
export default React.memo(MapWrapper)