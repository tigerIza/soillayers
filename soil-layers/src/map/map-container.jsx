import { Map, InfoWindow, Marker, Polygon} from 'google-maps-react';
 import React, {useState} from 'react';
 //import {Map} from './map'

export function MapContainer(props) {

	const [show, setShow] = useState(false);

	const mapStyle = {
		width: '450px',
		height: '450px',
		mapTypeId: 'terrain',
	}

	const triangleCoords = [
		{lat: -21.854885, lng: 127.081807},
		{lat: -21.854885, lng: 128.081807},
		{lat: -22.154885, lng: 128.281807},
		{lat: -22.154885, lng: 127.281807},
		{lat: -21.854885, lng: 127.081807} //last dot to close shape
	];

	return (
		<>
			<div >
				<Map 
					google={props.google} 
					zoom={7}
					style={mapStyle}
					initialCenter={{
						lat: -20.854885,
						lng: 128.081807
					}}
				>
					<Polygon
						paths={triangleCoords}
						strokeColor="#0000FF"
						strokeOpacity={0.8}
						strokeWeight={2}
						fillColor="#0000FF"
						fillOpacity={0.35}
						onClick={setShow(true)}
						/>
				</Map>
			</div>
			{show && 
				<div>
					<p>graph here</p>
				</div>
				}
		</>
	)
}