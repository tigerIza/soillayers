import React, {useState} from 'react';
import './styles/main.scss';
import MapWrapper from './map/map-wrapper';
import GraphArea from './graph/graph-area';

function App() {
  const [show, setShow] = useState(false);

  const isAreaClicked = (clicked) => {
    setShow(clicked);
  };

  return (
    <div className="app">
      <ol>
        <li>Google Maps with custom polygon to highlight area</li>
        <li>Chart to display soil layers with info</li>
      </ol>
      <div className="container">
        <MapWrapper areaClicked = {(clicked)=> isAreaClicked(clicked)}/>
        {show && 
          <GraphArea />
        }
      </div>
    </div>
  );
}

export default App;
